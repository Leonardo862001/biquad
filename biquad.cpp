#include<iostream>
#include<fstream>
#include<cstdlib>
#include <utility>
#include <unistd.h>
#include <octave/oct.h>
#include <octave/octave.h>
#include <octave/parse.h>
#include <octave/interpreter.h>
#include "biquad.hpp"
int main(){
	float f0, ft, Q, a;
	int n;
	biquad::biquad::filters type;
	std::cout << "1)for lowPass\n2)for highPass\n3)for bandPass\n4)for notch\n5)for allPass\n6)for peakingEQ\n7)for lowShelf\n8)for highShelf" << std::endl;
	std::cin >> n;
	std::cout << "f0 ft Q gainDB\n";
	std::cin >> f0 >> ft >> Q >> a;
	switch(n){
		case 1:
			type=biquad::filters::lowPass;
			break;
		case 2:
			type=biquad::filters::highPass;
			break;
		case 3:
			type=biquad::filters::bandPass;
			break;
		case 4:
			type=biquad::filters::bandStop;
			break;
		case 5:
			type=biquad::filters::allPass;
			break;
		case 6:
			type=biquad::filters::peakingEQ;
			break;
		case 7:
			type=biquad::filters::lowShelf;
			break;
		case 8:
			type=biquad::filters::highShelf;
			break;
		default:
			type=biquad::filters::lowPass;
	}
	biquad::coefficients c = createFilter(type, f0, ft, Q, a); 
#ifdef OCTAVE
	std::ofstream out;
	out.open("a.m");
	out << "function a()\n";
	out << "\tpkg load control\n";
	out << "\tz=tf(\'z\'," << 1/ft <<")\n";
	out << "\tf=(" << c.b0 << "+" << c.b1 << "*z^-1+" << c.b2 << "*z^-2)/(1+" << c.a1 << "*z^-1+" << c.a2 << "*z^-2)\n";
	out << "\tbode(f)\n";
	out << "\tpause\n";
	out << "end\n";
	out.close();

	pid_t pid = fork();

	if(pid != 0) // parent
	{
		std::cout << "parent, exiting\n";
	}
	else
	{
		// arguments for octave
		string_vector argv (2);
		argv(0) = "embedded";
		argv(1) = "-q"; // quiet

		// start octave, run embedded (third parameter == true)
		octave_main (2, argv.c_str_vec (), true);

		// read the script file
		source_file("a.m");

		// call the function with an argument
		feval("a");
		std::system("rm a.m");
		clean_up_and_exit(0);
	}
#endif
}
