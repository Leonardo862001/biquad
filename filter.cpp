#include<iostream>
#include<fstream>
#include "biquad.hpp"
#define SIZE 10000
#define K M_PI_2/100
#define STEP K/SIZE
#define FT SIZE/K
int main(){
	float in[3];
	float out[3];
	biquad::Biquad_chain<float, 5> filt(in, out, createFilter(biquad::filters::lowPass, 200, 44100, 0.3, 0));
	float a[SIZE];
	float j=0;
	for(int i = 0; i < SIZE; i++){
		a[i]=8*sin(2*M_PI*100*j)+sin(2*M_PI*2000*j);
		j+=STEP;
	}
#ifdef DEBUG
	std::cout << STEP << "\n" << FT << "\n";
#endif
	in[0]=a[0];
	in[1]=a[2];
	out[0]=out[1]=0;
#ifndef DEBUG
	std::cout << 0 << "\t" << in[0] << "\t" << out[0] << "\n";
	std::cout << STEP << "\t" << in[1] << "\t" << out[1] << "\n";
#endif
	for(int i = 2; i < SIZE; i++){
		in[2]=a[i];
		filt.evaluate();
#ifndef DEBUG
		std::cout << (float)STEP*i << "\t" << in[2] << "\t" << out[2] << "\n";
#endif
		in[0]=in[1];
		in[1]=in[2];
		out[0]=out[1];
		out[1]=out[2];
	}
}
