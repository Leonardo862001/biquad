#include <cmath>
namespace biquad{
	struct coefficients{
		float a1,a2,b0,b1,b2;
	};
	template<typename T>
		class Biquad{
			public:
				/*
				   Biquad(T* In, T* Out, coefficients coeff){
				   in = In;
				   out = Out;
				   c = coeff;
				   }
				   */
				void evaluate(){
					out[2]= (in[1]*c.b1+in[0]*c.b2+in[2]*c.b0)-(out[1]*c.a1+out[0]*c.a2);
				}
				void set(T* In, T* Out, coefficients coeff){
					in = In;
					out = Out;
					c = coeff;
				}
			private:
				T* in;
				T* out;
				coefficients c; // A1, A2, B0, B1, B2
		};
	template<typename T, int n>
		class Biquad_chain{
			public:
				Biquad_chain(T* In, T* Out, coefficients c){
					f[0].set(In, temp[0], c);
					for(int i = 1; i < n-1; i++){
						f[i].set(temp[i-1], temp[i], c);
					}
					f[n-1].set(temp[n-2], Out, c);
				}
				void evaluate(){
					for(int i = 0; i < n; i++){
						f[i].evaluate();
					}
					for(int i = 0; i < n; i++){
						temp[i][0]=temp[i][1];
						temp[i][1]=temp[i][2];
					}
				}
			private:
				Biquad<T> f[n];
				T temp[n-1][3] = {{0}};
		};
	enum class filters { lowPass, highPass, bandPass, bandStop, allPass, peakingEQ, lowShelf, highShelf };
	coefficients createFilter(filters type, float f0, float ft, float Q, float peakGaindB){
		float gain = pow(10, peakGaindB/20);
		float omega0 = 2*M_PI*(f0/ft);
		float alpha = sin(omega0)/(2*Q);
		coefficients c;
		switch(type){
			case filters::lowPass:
				{
					float a0 = 1 + alpha;
					c.a1=(-2*cos(omega0))/a0;
					c.a2=(1-alpha)/a0;
					c.b0=((1-cos(omega0))/2)/a0;
					c.b1=(1-cos(omega0))/a0;
					c.b2=((1-cos(omega0))/2)/a0;
				}
				break;
			case filters::highPass:
				{
					float a0 = 1 + alpha;
					c.a1=(-2*cos(omega0))/a0;
					c.a2=(1-alpha)/a0;
					c.b0=((1+cos(omega0))/2)/a0;
					c.b1=-(1+cos(omega0))/a0;
					c.b2=((1+cos(omega0))/2)/a0;
				}
				break;
			case filters::bandPass:
				{
					float a0 = 1 + alpha;
					c.a1=(-2*cos(omega0))/a0;
					c.a2=(1-alpha)/a0;
					c.b0=alpha/a0;
					c.b1=0;
					c.b2=-alpha/a0;
				}
				break;
			case filters::bandStop:
				{
					float a0 = 1 + alpha;
					c.a1=(-2*cos(omega0))/a0;
					c.a2=(1-alpha)/a0;
					c.b0=1/a0;
					c.b1=-(2*cos(omega0))/a0;
					c.b2=1/a0;
				}
				break;
			case filters::allPass:
				{
					float a0 = 1 + alpha;
					c.a1=(-2*cos(omega0))/a0;
					c.a2=(1-alpha)/a0;
					c.b0=(1-alpha)/a0;
					c.b1=(-2*cos(omega0))/a0;
					c.b2=(1+alpha)/a0;
				}
				break;
			case filters::peakingEQ:
				{
					float a0 = 1 + alpha/gain;
					c.a1=(-2*cos(omega0))/a0;
					c.a2=(1-alpha/gain)/a0;
					c.b0=(1+alpha*gain)/a0;
					c.b1=(-2*cos(omega0))/a0;
					c.b2=(1-alpha*gain)/a0;
				}
				break;
			case filters::lowShelf:
				{
					float a0 = (gain+1)+(gain-1)*cos(omega0)+2*sqrt(gain)*alpha;
					c.a1=(-2*((gain-1)+(gain+1)*cos(omega0)))/a0;
					c.a2=((gain+1)+(gain-1)*cos(omega0)-2*sqrt(gain)*alpha)/a0;
					c.b0=(gain*((gain+1)-(gain-1)*cos(omega0)+2*sqrt(gain)*alpha))/a0;
					c.b1=(2*gain*((gain-1)-(gain+1)*cos(omega0)))/a0;
					c.b2=(gain*((gain+1)-(gain-1)*cos(omega0)-2*sqrt(gain)*alpha))/a0;
				}
				break;
			case filters::highShelf:
				{
					float a0 = (gain+1)-(gain-1)*cos(omega0)+2*sqrt(gain)*alpha;
					c.a1=(2*((gain-1)+(gain+1)*cos(omega0)))/a0;
					c.a2=((gain+1)-(gain-1)*cos(omega0)-2*sqrt(gain)*alpha)/a0;
					c.b0=(gain*((gain+1)+(gain-1)*cos(omega0)+2*sqrt(gain)*alpha))/a0;
					c.b1=(-2*gain*((gain-1)-(gain+1)*cos(omega0)))/a0;
					c.b2=(gain*((gain+1)+(gain-1)*cos(omega0)-2*sqrt(gain)*alpha))/a0;
				}
				break;
		}
		return c;

	}
}
